import React, {useEffect} from 'react';
import {View, Image, Dimensions, Text, StyleSheet} from 'react-native';
import {PanGestureHandler} from 'react-native-gesture-handler';
import Animated, {
  useAnimatedGestureHandler,
  useSharedValue,
  useAnimatedStyle,
  withSpring,
  interpolate,
  Extrapolate,
  runOnJS,
} from 'react-native-reanimated';

export function HomeScreen({
  profiles: [mainProfile, ...remainingProfiles],
  deleteData,
}: {
  profiles: string[];
  deleteData: () => void;
}) {
  const x = useSharedValue(0);
  const width = Dimensions.get('screen').width;
  const height = Dimensions.get('screen').height;
  const gestureHandler = useAnimatedGestureHandler({
    onStart: (_, ctx: {startX: number}) => {
      ctx.startX = x.value;
    },
    onActive: (event, ctx) => {
      x.value = ctx.startX + event.translationX;
    },
    onEnd: _ => {
      if (x.value < -120) {
        runOnJS(deleteData)();
      } else if (x.value > 120) {
        runOnJS(deleteData)();
      } else {
        x.value = withSpring(0);
      }
    },
  });
  useEffect(() => {
    x.value = 0;
  }, [mainProfile]);
  const animatedStyle = useAnimatedStyle(() => {
    const rotate = interpolate(x.value, [-200, 200], [-45, 45]);
    return {
      transform: [
        {
          translateY: height / 2 + 20,
        },
        {
          translateX: x.value,
        },

        {
          rotateZ: rotate.toString() + ' deg',
        },
        {
          translateY: -height / 2 - 20,
        },
      ],
    };
  });
  const leftText = useAnimatedStyle(() => {
    const scale = interpolate(x.value, [-120, 0], [6, 0], {
      extrapolateLeft: Extrapolate.CLAMP,
    });
    const tranlateXvalue = interpolate(x.value, [-300, 0], [width / 2, -40]);
    const opacity = interpolate(x.value, [-120, 0], [1, 0]);
    return {
      transform: [
        {
          translateX: tranlateXvalue,
        },

        {
          scale: scale,
        },
      ],
      opacity: opacity,
    };
  });
  const rightText = useAnimatedStyle(() => {
    const scale = interpolate(x.value, [0, 120], [0, 6], {
      extrapolateLeft: Extrapolate.CLAMP,
    });
    const opacity = interpolate(x.value, [0, 120], [0, 1]);
    const tranlateXvalue = interpolate(
      x.value,
      [0, 300],
      [width + 40, width / 2],
    );
    return {
      transform: [
        {
          translateX: tranlateXvalue,
        },

        {
          scale: scale,
        },
      ],
      opacity: opacity,
    };
  });
  if (mainProfile === undefined) {
    return <Text>No more profiles available</Text>;
  }
  return (
    <View>
      <Animated.Text style={[leftText, style.Text, {top: height / 2}]}>
        🤕
      </Animated.Text>
      <View>
        {remainingProfiles.reverse().map((data, index) => {
          return (
            <Image
              key={index}
              style={[
                style.Card,
                style.PositionAbsolute,
                {
                  height: height - 200,
                  width: width - 100,
                },
              ]}
              source={{
                uri: data,
              }}
            />
          );
        })}
        <PanGestureHandler onGestureEvent={gestureHandler}>
          <Animated.Image
            style={[
              animatedStyle,
              {height: height - 200, width: width - 100},
              style.Card,
            ]}
            source={{
              uri: mainProfile,
            }}
          />
        </PanGestureHandler>
      </View>
      <Animated.Text style={[rightText, style.Text, {top: height / 2}]}>
        😎
      </Animated.Text>
    </View>
  );
}
const style = StyleSheet.create({
  Text: {
    position: 'absolute',
    zIndex: 4,
    color: 'red',
  },
  Card: {
    borderRadius: 10,
    marginTop: 60,
    alignSelf: 'center',
  },

  PositionAbsolute: {
    position: 'absolute',
  },
});
