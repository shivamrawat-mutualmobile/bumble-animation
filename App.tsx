import React, {useEffect, useState} from 'react';
import {HomeScreen} from './app/HomeScreen';
import {GestureHandlerRootView} from 'react-native-gesture-handler';

const App = () => {
  const [state, setState] = useState<string[]>([]);

  function deleteData() {
    const [_, ...profiles] = state;
    setState(profiles);
  }

  useEffect(() => {
    setState([
      'https://images.unsplash.com/photo-1604426633861-11b2faead63c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1964&q=80',
      'https://images.pexels.com/photos/1704488/pexels-photo-1704488.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
      'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=687&q=80',
    ]);
  }, []);

  return (
    <GestureHandlerRootView>
      <HomeScreen profiles={state} deleteData={deleteData} />
    </GestureHandlerRootView>
  );
};

export default App;
